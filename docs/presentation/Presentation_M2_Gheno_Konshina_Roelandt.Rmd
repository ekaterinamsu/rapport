---
title: "Division sociale de l'espace résidentiel <br /> Cas de la Zone Urbaine de Mexico"
subtitle: "Projet tutoré (Devoir transversal)<html><div style='float:left'></div><hr color='#EB811B' size=1px width=796px></html>"
author: "Mathieu Gheno, Ekaterina Konshina, Nicolas Roelandt"
date: "11 janvier 2017"
output:
  xaringan::moon_reader:
    chakra: libs/remark-latest.min.js
    css: [default, metropolis, metropolis-fonts]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      beforeInit: "libs/macros.js"
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

background-image: url('../images/Mapa_de_las_lagunas_rios_y_lugares_que_circundan_a_Mexico_(22341819185).jpg')


Image credit: [Fondo Antiguo de la Biblioteca de la Universidad de Sevilla](https://www.flickr.com/photos/fdctsevilla/22341819185/)

---

class: center, middle

# Département

### Master 2ème année Parcours Géomatique, géodécisionnel, géomarketing et multimédia (G2M)

![:scale 50%](../images/mainlogo.png)

---
class: inverse, center, middle

# Introduction

---
# Contexte

Données issues de l'[INEGI](http://www.inegi.org.mx/default.aspx):

* [Encuesta Intercensal 2015](http://www.inegi.org.mx/est/contenidos/proyectos/accesomicrodatos/encuestas/hogares/especiales/ei2015/) 

* [Censo de Población y Vivienda 2010](http://www.inegi.org.mx/est/contenidos/proyectos/accesomicrodatos/cpv2010/default.aspx)
---

# Préparation des données

## données 2015 
* 64 fichiers csv
* plus de 180 variables
* 1 personne = 1 ligne
* 1 vivienda = 1 ligne

Soit plus de 22 millions de lignes

## données 2010
* 32 fichiers xls
* agrégées au *Municipe*

---

# Préparation des données
### Filtrage 

Zone d'étude limitée à la Zone Urbaine de Mexico:
* 76 *municipes*:

---

# Préparation des données
## choix des variables

---
class: inverse, center, middle

# Analyse des données

---
# Analyse en Composantes Principales
---
# Analyse factorielle des correspondances

une diapo
---
# Analyse factorielle des correspondances
Une autre diapo
---
# Classification ascendante hiérarchique

---
class: inverse, center, middle

# Outils
---
# Outils
- Mise en forme des données: [Python/pandas](http://pandas.pydata.org/)
- Analyse: [R]()
- Rapport écrit: [liftr](https://liftr.me/)
- présentation de la soutenance: [Xaringan]()
- Site internet: [blogdown](https://bookdown.org/yihui/blogdown/)
---
class: inverse, center, middle

# Conclusion
---
# Conclusion

---
# Aller plus loin
## Analyse
- comparaison avec les données de 2010
- évaluer les populations impactées par le seisme de septembre 2017

## Maîtrise de R
- Visualisation et cartographie dynamiques: [R shiny](http://shiny.rstudio.com/) et [Leaflet](https://rstudio.github.io/leaflet/)
- manipulation des facteurs : [forcats](http://perso.ens-lyon.fr/lise.vaudor/manipulation-de-facteurs-avec-forcats/)
- programmation fonctionnelle: [purrr](http://colinfay.me/purrr-statistics)
- production éditoriale: [bookdown](https://bookdown.org/yihui/bookdown/)
---
class: center, middle, inverse

# Fin

**Merci pour votre attention**

Liens vers les dépôts : https://framagit.org/m2_projet_mexique

Lien vers le site: http://m2_projet_mexique.frama.io/website

{{---}}

Slides created via the R package [**xaringan**](https://github.com/yihui/xaringan).

The chakra comes from [remark.js](https://remarkjs.com), [**knitr**](http://yihui.name/knitr), and [R Markdown](https://rmarkdown.rstudio.com).