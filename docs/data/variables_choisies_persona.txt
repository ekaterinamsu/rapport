variable,explication
ID_PERSONA,identifiant de la personne
ID_VIV,identifiant de l'habitation (base PERSONA)
ENT,code Entidad Federativa
NOM_ENT, nom Entidad Federativa
MUN, code Municipio
NOM_MUN, Nom Municipio 
LOC50K, code localidad
NOM_LOC, code localidad
FACTOR,facteur de pondération de la persona
PERTE_INDIGENA,se considère comme indigène
HLENGUA,parle un dialecte indigène
HESPANOL,parle espagnol
NIVACAD,niveau de scolarité
ALFABET,alphabétisation
CONACT,statut d'activité
